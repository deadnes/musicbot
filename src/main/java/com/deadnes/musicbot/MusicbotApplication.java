package com.deadnes.musicbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.deadnes.musicbot.service.impl.MusicBot;

import uk.co.caprica.vlcj.discovery.NativeDiscovery;

@SpringBootApplication
public class MusicbotApplication {
	
	public static void main(String[] args) {
		new NativeDiscovery().discover();
		SpringApplication.run(MusicbotApplication.class, args);
		MusicBot.run();
	}
}
