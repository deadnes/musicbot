package com.deadnes.musicbot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deadnes.musicbot.model.Artist;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long>{
	
}
