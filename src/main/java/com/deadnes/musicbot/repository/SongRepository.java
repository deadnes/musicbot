package com.deadnes.musicbot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.model.Song;
import com.deadnes.musicbot.model.Artist;

@Repository
public interface SongRepository extends JpaRepository<Song, Long>{
	
	List<Song> findByGenre (Genre genre);
	
	List<Song> findByArtist (Artist artist);
	
	@Query(value = "SELECT s FROM Song s WHERE s.name LIKE CONCAT('%', :name, '%')")
	List<Song> findByName (@Param ("name") String name);
}
