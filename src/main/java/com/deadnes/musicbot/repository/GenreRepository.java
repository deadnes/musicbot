package com.deadnes.musicbot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.deadnes.musicbot.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>{
	
}
