package com.deadnes.musicbot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "genre")
public class Genre extends AbstractBaseEntity{
	
	private static final long serialVersionUID = 8454580730987980324L;
	
	@Column(nullable = false, unique = true, length = 20)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
