package com.deadnes.musicbot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "song")
public class Song extends AbstractBaseEntity{
	
	private static final long serialVersionUID = -2514449842253524131L;

	@Column(nullable = false, unique = true, length = 50)
	private String name;
	
	@Column(nullable = false, unique = true, length = 300)
	private String mrl;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "artist_id", nullable = true)
	private Artist artist;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "genre_id", nullable = true)
	private Genre genre;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMrl() {
		return mrl;
	}

	public void setMrl(String mrl) {
		this.mrl = mrl;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
}
