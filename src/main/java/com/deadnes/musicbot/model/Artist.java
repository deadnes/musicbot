package com.deadnes.musicbot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "artist")
public class Artist extends AbstractBaseEntity{
	
	private static final long serialVersionUID = -5163929005160903758L;
	
	@Column(nullable = false, unique = true, length = 50)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
