package com.deadnes.musicbot.service;

import com.deadnes.musicbot.model.Artist;

public interface ArtistService extends CrudService<Artist>{
}
