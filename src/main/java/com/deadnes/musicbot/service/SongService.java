package com.deadnes.musicbot.service;

import java.util.List;

import com.deadnes.musicbot.model.Artist;
import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.model.Song;

public interface SongService extends CrudService<Song>{
	List <Song> findByArtist (Artist artist);
	
	List<Song> findByGenre (Genre genre);
	
	List<Song> findByName (String name);
}
