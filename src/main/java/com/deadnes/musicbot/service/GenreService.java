package com.deadnes.musicbot.service;

import com.deadnes.musicbot.model.Genre;

public interface GenreService extends CrudService<Genre>{
}
