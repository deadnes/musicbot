package com.deadnes.musicbot.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadnes.musicbot.model.Artist;
import com.deadnes.musicbot.model.Song;
import com.deadnes.musicbot.repository.ArtistRepository;
import com.deadnes.musicbot.repository.SongRepository;
import com.deadnes.musicbot.service.ArtistService;

@Service
public class ArtistServiceImpl implements ArtistService{

	@Autowired
	private ArtistRepository artistRepository;
	
	@Autowired
	private SongRepository songRepository;
	
	@Override
	public Artist findOne(Long id) {
		Optional<Artist> artist = artistRepository.findById(id);
		if (artist.isPresent()) {
			return artist.get();
		}else {
			return null;
		}
	}
	
	@Override
	public List<Artist> findAll (){
		return artistRepository.findAll();
	}

	@Override
	@Transactional
	public Artist save(Artist artist) {
		return artistRepository.save(artist);
	}

	@Override
	@Transactional
	public void remove(Long id) throws IllegalArgumentException {
		Optional<Artist> artist = artistRepository.findById(id);
		if (!artist.isPresent()) {
			throw new IllegalArgumentException(
					String.format("ERROR: Trying to remove artist with nonexistent id=%d", id));
		}
		List<Song> songs = songRepository.findByArtist(artist.get());
		
		for (Song song : songs) {
			song.setArtist(null);
			songRepository.save(song);
		}
		
		artistRepository.deleteById(id);
	}

}
