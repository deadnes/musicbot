package com.deadnes.musicbot.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.model.Song;
import com.deadnes.musicbot.repository.GenreRepository;
import com.deadnes.musicbot.repository.SongRepository;
import com.deadnes.musicbot.service.GenreService;

@Service
public class GenreServiceImpl implements GenreService{

	@Autowired
	private GenreRepository genreRepository;
	
	@Autowired
	private SongRepository songRepository;
	
	@Override
	public Genre findOne(Long id) {
		Optional<Genre> genre = genreRepository.findById(id);
		if (genre.isPresent()) {
			return genre.get();
		}else {
			return null;
		}
	}

	@Override
	public List<Genre> findAll() {
		return genreRepository.findAll();
	}

	@Override
	@Transactional
	public Genre save(Genre genre) {
		return genreRepository.save(genre);
	}

	@Override
	@Transactional
	public void remove(Long id) throws IllegalArgumentException {
		Optional<Genre> genre = genreRepository.findById(id);
		if (!genre.isPresent()) {
			throw new IllegalArgumentException(
					String.format("ERROR: Trying to remove genre with nonexistent id=%d", id));
		}
		List<Song> songs = songRepository.findByGenre(genre.get());
		
		for (Song song : songs) {
			song.setGenre(null);
			songRepository.save(song);
		}
		
		genreRepository.deleteById(id);
		
	}
	
}
