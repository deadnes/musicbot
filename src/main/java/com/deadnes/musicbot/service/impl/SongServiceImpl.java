package com.deadnes.musicbot.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadnes.musicbot.model.Artist;
import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.model.Song;
import com.deadnes.musicbot.repository.SongRepository;
import com.deadnes.musicbot.service.SongService;

@Service
public class SongServiceImpl implements SongService{

	@Autowired
	private SongRepository songRepository;
	
	@Override
	public Song findOne(Long id) {
		Optional<Song> song = songRepository.findById(id);
		if (song.isPresent()) {
			return song.get();
		}else {
			return null;
		}
	}
	
	public List<Song> findByName (String name) {
		return songRepository.findByName(name);
	}

	@Override
	public List<Song> findAll() {
		return songRepository.findAll();
	}

	@Override
	@Transactional
	public Song save(Song song) {
		return songRepository.save(song);
	}

	@Override
	@Transactional
	public void remove(Long id) throws IllegalArgumentException {
		Optional<Song> song = songRepository.findById(id);
		if (!song.isPresent()) {
			throw new IllegalArgumentException(
					String.format("ERROR: Trying to remove song with nonexistent id=%d", id));
		}
		songRepository.deleteById(id);
	}

	@Override
	public List<Song> findByArtist(Artist artist) {
		return songRepository.findByArtist(artist);
	}

	@Override
	public List<Song> findByGenre(Genre genre) {
		return songRepository.findByGenre(genre);
	}
	
}
