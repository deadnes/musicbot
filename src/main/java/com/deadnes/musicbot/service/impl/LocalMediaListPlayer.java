package com.deadnes.musicbot.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.co.caprica.vlcj.component.AudioMediaPlayerComponent;
import uk.co.caprica.vlcj.medialist.MediaList;
import uk.co.caprica.vlcj.medialist.MediaListItem;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;

public final class LocalMediaListPlayer {
	
	private static final AudioMediaPlayerComponent mediaPlayerComponent = new AudioMediaPlayerComponent();
	private static final MediaList mediaList = mediaPlayerComponent.getMediaPlayerFactory().newMediaList();
	private int currentItemIndex = -1;
	private static LocalMediaListPlayer instance = null;
	
	private LocalMediaListPlayer () {
		initialize();
	}
	
	public static LocalMediaListPlayer getInstance (){
		if (instance == null) {
			instance = new LocalMediaListPlayer();
		}
		return instance;
	}
	
	public void initialize () {
		mediaPlayerComponent.getMediaPlayer().setAudioOutput("waveout");
		mediaPlayerComponent.getMediaPlayer().setAudioOutputDevice(null, "Line 1 (Virtual Audio Cable) ($1,$64)");
		mediaPlayerComponent.getMediaPlayer().setStandardMediaOptions("novideo");
		mediaPlayerComponent.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
			@Override
		    public void playing(MediaPlayer mediaPlayer) {
				if (mediaPlayer.subItemCount() > 0) {
					System.out.println("Playing..." + mediaPlayer.getSubItemMediaMetaData().get(0).getTitle());
					MusicBot.setNowPlaying(mediaPlayer.getSubItemMediaMetaData().get(0).getTitle());
					MusicBot.setSource("[URL]" + mediaPlayer.mrl() + "[/URL]");
				}else {
					System.out.println("Playing..." + mediaPlayer.getMediaMetaData().getTitle());
					MusicBot.setNowPlaying(mediaPlayer.getMediaMetaData().getTitle());
					MusicBot.setSource("local file");
				}
		    }
			@Override
		    public void paused(MediaPlayer mediaPlayer) {
				System.out.println("Paused...");
			}
		    @Override
		    public void stopped(MediaPlayer mediaPlayer) {
		    	System.out.println("Stopped...");
		    	playNext();
		    }
		    @Override
		    public void finished(MediaPlayer mediaPlayer) {
		    	System.out.println("Finished...");
		    	if (mediaPlayer.subItemCount() > 0 && mediaPlayer.subItemIndex() == -1) {
		    		mediaPlayer.playNextSubItem();
		    	}
		    }
		    @Override
		    public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
		    	System.out.println("Title changed...");
		    	System.out.println("New title = " + newTitle);
		    }
		    @Override
		    public void error(MediaPlayer mediaPlayer) {
		    	System.out.println("Error...");
		    }
		    @Override
		    public void newMedia(MediaPlayer mediaPlayer) {
		    	System.out.println("New Main Media Opened..." + mediaPlayer.getMediaMetaData().getTitle());
		    }
		    @Override
		    public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
		    	System.out.println("New Sub Item Playback Begun...");
		    	System.out.println("Sub Item Index = " + subItemIndex);
		    }
		    @Override
		    public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
		    	System.out.println("Sub Item Finished...");
		    	System.out.println("Sub Item Index = " + subItemIndex);
		    }
		});
	}
	
	public void play (String mrl) {
		mediaPlayerComponent.getMediaPlayer().playMedia(mrl);
	}
	
	public void playNext () {
		if (mediaList.size() > 0) {
			currentItemIndex++;
			if (mediaList.size() == currentItemIndex) {
				currentItemIndex = 0;
			}
			mediaPlayerComponent.getMediaPlayer().playMedia(mediaList.items().get(currentItemIndex).mrl());
		}
	}
	
	public void playPrevious () {
		if (mediaList.size() > 0) {
			currentItemIndex--;
			if (currentItemIndex <= -1) {
				currentItemIndex = mediaList.size() - 1;
			}
			mediaPlayerComponent.getMediaPlayer().playMedia(mediaList.items().get(currentItemIndex).mrl());
		}
	}
	
	public void savePlaylistToFile (String playlistName) {
		try {
			if (!Files.exists(Paths.get(".\\playlists"))) {
				Files.createDirectory(Paths.get(".\\playlists"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			List <String> items = new ArrayList<>();
			for (MediaListItem item : mediaList.items()) {
				items.add(item.mrl());
			}
			Files.write(Paths.get(".\\playlists", playlistName + ".playlist"), items);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadPlaylistFromFile (String playlistName) {
		List <String> items = new ArrayList<>();
		try {
			items = Files.readAllLines(Paths.get(".\\playlists", playlistName + ".playlist"));
			currentItemIndex = -1;
			mediaList.clear();
			for (String item : items) {
				mediaList.addMedia(item);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadPlaylistFromFIle (int playlistNumber) {
		List<Path> names;
		try {
			names = Files.list(Paths.get(".\\playlists")).collect(Collectors.toList());
			int itemIndex = 0;
			for (Path name : names) {
				if (name.getFileName().toString().endsWith(".playlist")) {
					itemIndex++;
					if (itemIndex == playlistNumber) {
						loadPlaylistFromFile(name.getFileName().toString().substring(0, name.getFileName().toString().length() - 9));
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stop () {
		mediaPlayerComponent.getMediaPlayer().stop();
	}
	
	public void pause () {
		mediaPlayerComponent.getMediaPlayer().pause();
	}
	
	public void resume () {
		mediaPlayerComponent.getMediaPlayer().play();
	}
	
	public void addToPlaylist (String mrl) {
		mediaList.addMedia(mrl);
	}
	
	public void removeFromPlaylist (int mediaIndex) {
		if (mediaIndex > -1 && mediaIndex < mediaList.size()) {
			mediaList.removeMedia(mediaIndex);	
		}
	}
	
	public void exit () {
		mediaPlayerComponent.release();
		System.exit(0);
	}
	
	public String getTitle () {
		if (mediaPlayerComponent.getMediaPlayer().getMediaMetaData().getTitle().startsWith("watch?v=")) {
			while(mediaPlayerComponent.getMediaPlayer().getSubItemMediaMetaData().isEmpty());
			return mediaPlayerComponent.getMediaPlayer().getSubItemMediaMetaData().get(0).getTitle();
		}else {
			return mediaPlayerComponent.getMediaPlayer().getMediaMetaData().getTitle();
		}
	}
	
	public String getPlaylistsNames () {
		String formattedNames = "Playlists:";
		List<Path> names;
		try {
			names = Files.list(Paths.get(".\\playlists")).collect(Collectors.toList());
			int itemIndex = 0;
			for (Path name : names) {
				if (name.getFileName().toString().endsWith(".playlist")) {
					itemIndex++;
					formattedNames += "\n" + itemIndex + ": " + name.getFileName().toString().substring(0, name.getFileName().toString().length() - 9);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return formattedNames;
	}
	
	public String getPlaylistItemNames(){
		if (mediaList.size() == 0) {
			return "Empty Playlist";
		}
		String formattedNames = "Current playlist:";
		int itemIndex = 0;
		for (MediaListItem item : mediaList.items()) {
			formattedNames += "\n" + (itemIndex+1) + ": ";
			if (itemIndex == currentItemIndex) {
				formattedNames += "[b]";
			}
			if (item.name().startsWith("watch?v=")){
				if (item.subItems().isEmpty()) {
					formattedNames += "[URL]" + item.mrl() + "[/URL]";
				}else {
					formattedNames += item.subItems().get(0).name();
				}
			}else {
				formattedNames += item.name();
			}
			if (itemIndex == currentItemIndex) {
				formattedNames += "[/b]";
			}
			itemIndex++;
		}
		return formattedNames;
	}
}
