package com.deadnes.musicbot.service;

import java.util.List;

import com.deadnes.musicbot.model.AbstractBaseEntity;

public interface CrudService<T extends AbstractBaseEntity> {
	T findOne (Long id);
	
	List<T> findAll ();
	
	T save (T t);
	
	void remove (Long id) throws IllegalArgumentException;
}
