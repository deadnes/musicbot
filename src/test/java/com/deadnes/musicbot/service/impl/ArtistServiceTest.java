package com.deadnes.musicbot.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.deadnes.musicbot.model.Artist;
import com.deadnes.musicbot.service.ArtistService;

public class ArtistServiceTest extends AbstractServiceTest{
	
	@Autowired
	private ArtistService artistService;
	
	private Artist artist;
	
	@Before
	public void setUp() {
		artist = new Artist();
		artist.setName("Test Artist");
		artistService.save(artist);
	}
	
	@Test
	public void testFindOne() {
		Artist artistToFind = artistService.findOne(artist.getId());
		assertNotNull(artistToFind);
	}
	
	@Test
	public void testFindAll() {
		List<Artist> artists = artistService.findAll();
		assertNotNull(artists);
		assertTrue(artists.size() > 0);
	}

	@Test
	public void testRemove() {
		artistService.remove(artist.getId());
		Artist removed = artistService.findOne(artist.getId());
		assertNull(removed);
	}
}
