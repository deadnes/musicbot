package com.deadnes.musicbot.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.deadnes.musicbot.model.Artist;
import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.model.Song;
import com.deadnes.musicbot.service.ArtistService;
import com.deadnes.musicbot.service.GenreService;
import com.deadnes.musicbot.service.SongService;

public class SongServiceTest extends AbstractServiceTest{
	
	@Autowired
	private SongService songService;
	
	@Autowired
	private GenreService genreService;
	
	@Autowired
	private ArtistService artistService;
	
	private Song song1;
	private Artist artist1;
	private Genre genre1;
	private Song song2;
	private Artist artist2;
	private Genre genre2;
	
	@Before
	public void setUp() {
		artist1 = new Artist();
		artist1.setName("Artist 1");
		artistService.save(artist1);
		genre1 = new Genre();
		genre1.setName("Genre 1");
		genreService.save(genre1);
		song1 = new Song();
		song1.setName("Song 1");
		song1.setMrl("D://test//test1.mp3");
		song1.setArtist(artist1);
		song1.setGenre(genre1);
		songService.save(song1);
		
		artist2 = new Artist();
		artist2.setName("Artist 2");
		artistService.save(artist2);
		genre2 = new Genre();
		genre2.setName("Genre 2");
		genreService.save(genre2);
		song2 = new Song();
		song2.setName("Song 2");
		song2.setMrl("D://test//test2.mp3");
		song2.setArtist(artist2);
		song2.setGenre(genre2);
		songService.save(song2);
	}
	
	@Test
	public void testFindOne() {
		Song songToFind = songService.findOne(song1.getId());
		assertNotNull(songToFind);
	}
	
	@Test
	public void testFindByGenre() {
		List<Song> songsByGenre = songService.findByGenre(genre1);
		Assert.assertNotNull(songsByGenre);
		Assert.assertTrue(songsByGenre.size() == 1 && songsByGenre.get(0).getGenre().equals(genre1));
		
		songsByGenre = songService.findByGenre(genre2);
		Assert.assertNotNull(songsByGenre);
		Assert.assertTrue(songsByGenre.size() == 1 && songsByGenre.get(0).getGenre().equals(genre2));
	}
	
	@Test
	public void testFindByArtist() {
		List<Song> songsByArtist = songService.findByArtist(artist1);
		Assert.assertNotNull(songsByArtist);
		Assert.assertTrue(songsByArtist.size() == 1 && songsByArtist.get(0).getArtist().equals(artist1));
		
		songsByArtist = songService.findByArtist(artist2);
		Assert.assertNotNull(songsByArtist);
		Assert.assertTrue(songsByArtist.size() == 1 && songsByArtist.get(0).getArtist().equals(artist2));
	}
	
	@Test
	public void testFindAll() {
		List<Song> songs = songService.findAll();
		assertNotNull(songs);
		assertTrue(songs.size() > 0);
	}
	
	@Test
	public void testFindByName() {
		List<Song> songs = songService.findByName("song 1");
		assertNotNull(songs);
		assertTrue(songs.size() > 0);
	}

	@Test
	public void testRemove() {
		songService.remove(song1.getId());
		Song removed = songService.findOne(song1.getId());
		assertNull(removed);
	}
	
}
