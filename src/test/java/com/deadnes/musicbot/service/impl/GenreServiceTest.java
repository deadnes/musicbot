package com.deadnes.musicbot.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.deadnes.musicbot.model.Genre;
import com.deadnes.musicbot.service.GenreService;

public class GenreServiceTest extends AbstractServiceTest{
	@Autowired
	private GenreService genreService;
	
	private Genre genre;
	
	@Before
	public void setUp() {
		genre = new Genre();
		genre.setName("Test Genre");
		genreService.save(genre);
	}
	
	@Test
	public void testFindOne() {
		Genre genreToFind = genreService.findOne(genre.getId());
		assertNotNull(genreToFind);
	}
	
	@Test
	public void testFindAll() {
		List<Genre> genres = genreService.findAll();
		assertNotNull(genres);
		assertTrue(genres.size() > 0);
	}

	@Test
	public void testRemove() {
		genreService.remove(genre.getId());
		Genre removed = genreService.findOne(genre.getId());
		assertNull(removed);
	}
}
